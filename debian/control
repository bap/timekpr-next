Source: timekpr-next
Section: admin
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Anthony Fok <foka@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-python,
               gir1.2-gtk-3.0,
               libgtk-3-0,
               po-debconf,
               python3,
               python3-dbus,
               python3-gi
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/timekpr-next
Vcs-Git: https://salsa.debian.org/python-team/packages/timekpr-next.git
Homepage: https://launchpad.net/timekpr-next/

Package: timekpr-next
Architecture: any
Depends: gir1.2-gtk-3.0,
         libgtk-3-0,
         lxpolkit | lxqt-policykit | mate-polkit | policykit-1-gnome | polkit-kde-agent-1 | polkit-1-auth-agent,
         polkitd, pkexec,
         python3,
         python3-dbus,
         python3-gi,
         python3-psutil (>= 3.4),
         systemd | systemd-shim,
         ${misc:Depends},
         ${python3:Depends}
Recommends: gir1.2-ayatanaappindicator3-0.1,
Suggests: espeak,
          gnome-shell-extension-appindicator,
          python3-espeak
Conflicts: timekpr-next-beta
Provides: timekpr
Replaces: timekpr,
          timekpr-beta,
          timekpr-next-beta
Description: Keep control of computer usage
 Timekpr-nExT is a fresh, simple and easy-to-use time managing software
 that helps optimizing time spent at computer for Your subordinates,
 children or even for Yourself.
 .
 The software is targeted at parents or supervisors to optimize / limit
 time spent at computer as they see fit.
 .
 Please report any bugs to Timekpr-nExT’s bug tracker on Launchpad at:
 https://bugs.launchpad.net/timekpr-next/+bugs
